#include <iostream>

using namespace std;

int findFact(int n)
{
    int factorial = 1;
    for(int i = 1; i <= n; i++)
        factorial *= i;

    return factorial;
}

int findNpR(int n, int r)
{
    return findFact(n) / findFact(n - r);
}

int findNcR(int n, int r)
{
    return findFact(n) / (findFact(n - r) * findFact(r));
}

int main()
{
    int n, r, nPr, nCr;

    cout << "Masukkan nilai n:" << endl;
    cin >> n;

    cout << "Masukkan nilai r:" << endl;
    cin >> r;

    nPr = findNpR(n, r);
    nCr = findNcR(n, r);

    cout << "Permutasi,nPr : "<< nPr << endl;
    cout << "Kombinasi,nCr : "<< nCr << endl;
} 
