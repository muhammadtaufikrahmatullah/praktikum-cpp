#include <iostream>
#include <string>

using namespace std;

int main() {
    string namaKaryawan;
    char golongan;
    char status;
    int gajiPokok;
    int tunjangan;
    int potonganIuran;
    int gajiBersih;

    cout << "Nama karyawan: ";
    getline(cin, namaKaryawan);

    cout << "Golongan (A/B): ";
    cin >> golongan;

    cout << "Status (Nikah/Belum): ";
    cin >> status;

    if (golongan == 'A') {
        gajiPokok = 200000;
        if (status == 'Nikah') {
            tunjangan = 50000;
        } else {
            tunjangan = 25000;
        }
    } else if (golongan == 'B') {
        gajiPokok = 350000;
        if (status == 'Nikah') {
            tunjangan = 75000;
        } else {
            tunjangan = 60000;
        }
    }

    cout << "Gaji Pokok: " << gajiPokok << endl;
    cout << "Tunjangan: " << tunjangan << endl;

    int gatun = gajiPokok + tunjangan;

    if (gatun <= 300000) {
        potonganIuran = gatun * 0.05;
    } else {
        potonganIuran = gatun * 0.1;
    }

    cout << "Potongan iuran: " << potonganIuran << endl;

    gajiBersih = gatun - potonganIuran;

    cout << "Gaji Bersih: " << gajiBersih << endl;

    return 0;
}
