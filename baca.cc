/*
   Nama Program : baca.cc
   Tgl buat     : 3 Oktober 2023
   Deskripsi    : contoh membaca nilai dan kemudian
                  menampilkan nilai yang dibaca
*/

#include <iostream>
#include <stdlib.h>

using namespace std;

int main()
{
    system("clear");

    int i = 0;
    float j = 0.0;
    char kar = ' ';
    string NString = "";

    cout << "Contoh membaca dan menulis" << endl;
    cout << "Masukkan nilai integer  : ";
    cin >> i;
    cout << "Masukkan nilai real     : ";
    cin >> j;
    cout << "Masukkan nilai karakter : ";
    cin >> kar;
    cout << "Masukkan nilai string   : ";
    cin >> NString;
    cout << "Nilai integer yang dibaca  = " << i << endl;
    cout << "Nilai real yang dibaca     = " << j << endl;
    cout << "Nilai karakter yang dibaca = " << kar << endl;
    cout << "Nilai string yang dibaca   = " << NString << endl;

    return 0;
}
